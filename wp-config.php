<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'slicedbread');

/** MySQL database username */
define('DB_USER', 'slicedbread');

/** MySQL database password */
define('DB_PASSWORD', '&,{J#e|6{^,Da+;oUHKoXnf~&');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gqijIP-113kjKQJjL/H}4)-!v?Vg.|t6Lm;n=.M>xza16S`W@2oo2I2j14tO}.%s');
define('SECURE_AUTH_KEY',  '+fD({BEZF|}bBsWrg:TM_mVv{{0pojX8/:Tw]1Xx7CCQUQ2g{uq?5Y($a0Lu6ew1');
define('LOGGED_IN_KEY',    '3F^-qMasB`PWA-0nNHu!Gd}&:Rmd9FZM[jQ4e!E6M:.]ji)wPXwV?|5EMi[l,o#o');
define('NONCE_KEY',        'wIuE4mZ{ejQ{k!N+ GUM-tfRE@h[j~Tr5TV>i~5p;Sw1YoyN8ST #)*{ X7Lgqb/');
define('AUTH_SALT',        ',jR$?M*t~]( BSBRP~.aWIxe p.3;H{Le)js!VTNA$IG/cX;l-1x!lq K[h7P=zp');
define('SECURE_AUTH_SALT', 'ZWhXN~9pk?^=mpr^>z@}+-B3QbT^5Zcc&Y|jl] ,8?)4:>M4{AfGJu7>o9Bi_?ZE');
define('LOGGED_IN_SALT',   '= `JH<fRH5%o[z%n)#a[H^;&N6@L:.-~b5Iv9dBOlX1PrHj?b7gehWwiARC#V#$M');
define('NONCE_SALT',       '/X]X%ja&P>ot*co @)_oM7Gh2M$-R!RSTBY+s_@P=3G1F8Td}z%h)-IgS5#b1UJg');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

