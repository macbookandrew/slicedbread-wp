<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Clean up archive title
 * @param  string $title archive title
 * @return string archive title
 */
function sb_remove_archive( $title ) {
    return str_replace( 'Archives: ', '', $title );
}
add_filter( 'get_the_archive_title', 'sb_remove_archive' );

/**
 * Get author bio
 * @return string HTML content
 */
function sb_get_author_bio() { ?>

    <div class="author-info">
        <p><?php echo get_avatar( get_the_author_meta( 'ID' ), 120 ) . get_the_author_meta( 'description' ); ?></p>
    </div>

    <?php
}

/**
 * Allow SVG file uploads
 * @param  array $mime_types array of allowed mime types
 * @return array modified array
 */
function sb_mime_types( $mime_types ) {
    $mime_types['svg'] = 'image/svg+xml';
    return $mime_types;
}
add_filter( 'upload_mimes', 'sb_mime_types' );
